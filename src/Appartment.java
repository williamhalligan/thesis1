


import java.sql.Date;

public class Appartment {

	private int acc_id;
	private String address;
	private String house_num;
	private String num_of_Bedrooms_available;
	private String num_Bedrooms_in_House;
	private String rent_Per_Week_OR_Month;
	private String deposit_required;
	private String landlords_first_Name;
	private String landlords_Surname;
	private String mobile_Num;
	private String fname;
	private String lname;
	private String username;
	private String password;
	private String sex;
	private String jobORcourse;
	private String gAccount;
	private String age;
	private String gPassword;
	private String paragraph_about_self;   
	private String user;       
	
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Appartment(int acc_id, String address, String house_num,
		String num_of_Bedrooms_available, String num_Bedrooms_in_House, String rent_Per_Week_OR_Month,
		String deposit_required, String landlords_first_Name, String landlords_Surname, String mobile_Num,
		String fname, String lname, String username, String password, String sex, String jobORcourse, 
		String gAccount, String age, String gPassword, String paragraph_about_self, String user  ) {
		super();
		this.acc_id = acc_id;
		this.address = address;
		this.house_num = house_num;
		this.num_of_Bedrooms_available = num_of_Bedrooms_available;
		this.num_Bedrooms_in_House = num_Bedrooms_in_House;
		this.rent_Per_Week_OR_Month = rent_Per_Week_OR_Month;
		this.deposit_required = deposit_required;
		this.landlords_first_Name = landlords_first_Name;
		this.landlords_Surname =landlords_Surname;
		this.mobile_Num = mobile_Num;
		this.fname = fname;
		this.lname = lname;
		this.username = username;
		this.password = password;
		this.sex = sex;
		this.jobORcourse = jobORcourse;
		this.gAccount = gAccount;
		this.age = age;
		this.gPassword = gPassword;
		this.paragraph_about_self = paragraph_about_self;
		this.user=user;

	}

	public int getacc_id() {
		return acc_id;
	}

	public void setId(int acc_id) {
		this.acc_id = acc_id;
	}

	public String getaddress() {
		return address;
	}

	public void setaddress(String address) {
		this.address = address;
	}

	public String gethouse_num() {
		return house_num;
	}

	public void sethouse_num(String house_num) {
		this.house_num = house_num;
	}

	public String getfname() {
		return fname;
	}

	public void setfname(String fname) {
		this.fname = fname;
	}
	public String getNum_of_Bedrooms_available() {
		return num_of_Bedrooms_available;
	}

	public void setNum_of_Bedrooms_available(String num_of_Bedrooms_available) {
		this.num_of_Bedrooms_available = num_of_Bedrooms_available;
	}

	public String getNum_Bedrooms_in_House() {
		return num_Bedrooms_in_House;
	}

	public void setNum_Bedrooms_in_House(String num_Bedrooms_in_House) {
		this.num_Bedrooms_in_House = num_Bedrooms_in_House;
	}

	public String getRent_Per_Week_OR_Month() {
		return rent_Per_Week_OR_Month;
	}

	public void setRent_Per_Week_OR_Month(String rent_Per_Week_OR_Month) {
		this.rent_Per_Week_OR_Month = rent_Per_Week_OR_Month;
	}

	public String getDeposit_required() {
		return deposit_required;
	}

	public void setDeposit_required(String deposit_required) {
		this.deposit_required = deposit_required;
	}

	public String getLandlords_first_Name() {
		return landlords_first_Name;
	}

	public void setLandlords_first_Name(String landlords_first_Name) {
		this.landlords_first_Name = landlords_first_Name;
	}

	public String getLandlords_Surname() {
		return landlords_Surname;
	}

	public void setLandlords_Surname(String landlords_Surname) {
		this.landlords_Surname = landlords_Surname;
	}

	public String getMobile_Num() {
		return mobile_Num;
	}

	public void setMobile_Num(String mobile_Num) {
		this.mobile_Num = mobile_Num;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getJobORcourse() {
		return jobORcourse;
	}

	public void setJobORcourse(String jobORcourse) {
		this.jobORcourse = jobORcourse;
	}

	public String getgAccount() {
		return gAccount;
	}

	public void setgAccount(String gAccount) {
		this.gAccount = gAccount;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		if (age!=null){
				this.age = age;
		}
	}

	public String getgPassword() {
		return gPassword;
	}

	public void setgPassword(String gPassword) {
		this.gPassword = gPassword;
	}

	public String getParagraph_about_self() {
		return paragraph_about_self;
	}

	public void setParagraph_about_self(String paragraph_about_self) {
		this.paragraph_about_self = paragraph_about_self;
	}
	
	@Override
	public String toString() {
		return String
				.format("Appartment [acc_id=%s, address=%s, house_num=%s, fname=%s"
						+ "num_of_Bedrooms_available=%s, num_Bedrooms_in_House=%s, rent_Per_Week_OR_Month=%s,"
						+ "deposit_required=%s, landlords_first_Name=%s, landlords_Surname=%s, mobile_Num=%s, "
						+ "fname=%s, lname=%s, username=%s, password=%s, sex=%s, jobORcourse=%s, gAccount=%s, "
						+ "age=%s, gPassword=%s, paragraph_about_self=%s]",
						acc_id, address, house_num, num_of_Bedrooms_available, num_Bedrooms_in_House, 
						rent_Per_Week_OR_Month, deposit_required, landlords_first_Name, landlords_Surname, 
						mobile_Num, fname, lname, username, password, sex, jobORcourse, gAccount, age,
						gPassword, paragraph_about_self);
	}
	   			
}

