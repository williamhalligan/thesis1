

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.table.AbstractTableModel;


public class AppartmentTableModel extends AbstractTableModel {
	public AppartmentTableModel(int i){}
	public static final int OBJECT_COL = -1;
	private static final int fname_COL = 0;
	private static final int lname_COL = 1;	
	private static final int Address_COL = 2;
	private static final int house_num_COL = 3;
	private static final int num_of_Bedrooms_available_COL = 4;
	private static final int num_Bedrooms_in_House_COL = 5;
	private static final int rent_Per_Week_OR_Month_COL = 6;
	private static final int deposit_required_COL = 7;
	private static final int landlords_first_Name_COL = 8;
	private static final int landlords_Surname_COL = 9;
	private static final int mobile_Num_COL = 10;
	private static final int sex_COL = 11;
	private static final int jobORcourse_COL = 12;
	private static final int age_COL = 13;
	private static final int paragraph_about_self_COL = 14;


	private String[] columnNames = { "First Name", "Last Name", "Address", "House Number",
			"Number of Bedrooms Available", "Number of Bedrooms in House", "Rent per week", 
			"Deposit", "Landlords First Name", "Landlords Surname", "Landlords Mobile Number",
			"Sex", "Job/Course", "Age", "Paragraph about self"};

	private List<Appartment> appartment;
	
	public AppartmentTableModel(List<Appartment> theAppartments) {
		appartment = theAppartments;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return appartment.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {

		Appartment tempAppartment = appartment.get(row);

		switch (col) {
		case fname_COL:
			return tempAppartment.getfname();
		case lname_COL: 
			return tempAppartment.getLname();
		case Address_COL:
			return tempAppartment.getaddress();
		case house_num_COL:
			return tempAppartment.gethouse_num();
		case num_of_Bedrooms_available_COL:
			return tempAppartment.getNum_of_Bedrooms_available();
		case num_Bedrooms_in_House_COL:
			return tempAppartment.getNum_Bedrooms_in_House();
		case rent_Per_Week_OR_Month_COL:
			return tempAppartment.getRent_Per_Week_OR_Month();
		case deposit_required_COL:
			return tempAppartment.getDeposit_required();
		case landlords_first_Name_COL:
			return tempAppartment.getLandlords_first_Name();
		case landlords_Surname_COL:
			return tempAppartment.getLandlords_Surname();
		case mobile_Num_COL:
			return tempAppartment.getMobile_Num();
		case sex_COL:
			return tempAppartment.getSex();
		case jobORcourse_COL:
			return tempAppartment.getJobORcourse();
		case age_COL:
			 if(tempAppartment.getAge()!=null){
			return tempAppartment.getAge();
			 }else {
				 return "Null";
			 }
		case paragraph_about_self_COL:
			return tempAppartment.getParagraph_about_self();
		case OBJECT_COL:
			return tempAppartment;
		default:
			return tempAppartment.getaddress();
		}
	}

	@Override
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}
	
}
