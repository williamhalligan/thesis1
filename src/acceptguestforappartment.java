import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.JLabel;

import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
public class acceptguestforappartment extends JFrame {
	private static  Connection connect;
	private JLabel title = new JLabel();
	private JPanel contentPane;
	private JTextField lastNameTextField;
	private JButton btnSearch;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton appartmentformbutton = new JButton();
	private JButton selectbutton = new JButton();
	private JButton logoutexitbutton = new JButton();
	private JComboBox  addresscb = new JComboBox();
	private JComboBox  sexcb = new JComboBox();
	private JComboBox  agecb = new JComboBox();
	private JComboBox coursecb = new JComboBox();
	private JComboBox jobcb = new JComboBox();	
	static String loginusername;
	static String password;
	
	
	JPanel pan = new JPanel();
	JPanel panel = new JPanel();
	
	private AppartmentDAO AppartmentDAO;
	List<Appartment> appartments = null;
	static acceptguestforappartment searchouse = new acceptguestforappartment(loginusername, password);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					searchouse.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public acceptguestforappartment(String loginusername, String password) {	
		 this.loginusername=loginusername;
	        if(loginusername!=null && password!=null){
	        System.out.print(loginusername +", "+password);
	        }
		// create the DAO
		try {
			AppartmentDAO = new AppartmentDAO();
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
		}
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Search for house");
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(204, 204, 255));
		setBounds(0, 0, 1370, 740);
		setResizable(false);
		contentPane = new JPanel();
	    contentPane.setBackground(new java.awt.Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
        title.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        title.setText("               Looking for Appartment");
        JPanel panel = new JPanel();
        //GridBagConstraints c = new GridBagConstraints();
		panel.setBackground(new java.awt.Color(204, 204, 255));
		//FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		//flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		pan.setBackground(new java.awt.Color(204, 204, 255));
		
		
		
		JLabel lblEnterLastName = new JLabel("Search by username, first name, last name, sex, landlords first name, landlords surname, sex, address and job or course just enter one of the following");
		//
		
		
		
		
		//panel.add(title);
		//panel.add(lblEnterLastName);
		//GridBagConstraints gbc= new GridBagConstraints();
		//panel.setLayout(new GridBagLayout());
	    
		pan.add(title);
	    panel.add(lblEnterLastName);

		lastNameTextField = new JTextField();
		panel.add(lastNameTextField);
		lastNameTextField.setColumns(10);	
		
		appartmentformbutton.addActionListener(new java.awt.event.ActionListener() 
        {
        	public void actionPerformed(java.awt.event.ActionEvent evt) 
        	{
        		Component frame = null;

				int response = JOptionPane.showConfirmDialog(
						acceptguestforappartment.this, "Log Out", "Confirm", 
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        				
        				
        				if (response == JOptionPane.YES_OPTION) 
        				{
        					searchouse.dispose();
            				houseform  cancellogin = new houseform(loginusername,  password);
            				cancellogin.setVisible(true);
        				}
        			}
        	
        });
		
		logoutexitbutton.addActionListener(new java.awt.event.ActionListener() 
        {
        	public void actionPerformed(java.awt.event.ActionEvent evt) 
        	{
        		Component frame = null;

				int response = JOptionPane.showConfirmDialog(
						acceptguestforappartment.this, "Log Out", "Confirm", 
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        				
        				
        				if (response == JOptionPane.YES_OPTION) 
        				{
        					searchouse.dispose();
            				intro  cancellogin = new intro();
            				cancellogin.setVisible(true);
        				}
        			}
        	
        });
		addresscb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Location", 
        		"Willow View", "Carton Drive", "Ballykeeran", "Moydrum",
        		"Valley Court", "Auburn Heights", "Cypress Gardens", "Old Willow Park", "Old Willow Place",
        		"Willow Park", "Wellmount Apartments", "Erris Grove, Willow Park", "Willow Green",
        		"Cartontroy", "Crystal Village", "Willow Grove", "Willow Cresent",
        		"Thornbury Drive", "Willow Place, Willow Park", "Willow View", "Willow Drive",
        		"Small Apartments", "Other"}));
        sexcb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sex", "Male", "Female"}));
        agecb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Age", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40"}));
        coursecb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Course", "Software Design", "Cooking", "Science" }));
        jobcb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Job", "Guard", "Teacher", "Farmer" }));
        appartmentformbutton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        appartmentformbutton.setText("<html>Appartment<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Form</html>");
        logoutexitbutton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        logoutexitbutton.setText("Log Out");
        selectbutton.setFont(new java.awt.Font("Tahoma", 1, 11)); 
        selectbutton.setText("Accept User");
        
        
      
        
        
        
        
        
        
        btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field
				// Call DAO and get appartments for the last name
				// If last name is empty, then get all appartments
				// Print out appartments				
				try {
					String lastName = lastNameTextField.getText();
					List<Appartment> appartments = null;
					if (lastName != null && lastName.trim().length() > 0) {
						appartments = AppartmentDAO.searchAppartments(lastName);
					} else {
						appartments = AppartmentDAO.getAllAppartments();
					}
					// create the model and update the "table"
					AppartmentTableModel model = new AppartmentTableModel(appartments);
					table.setModel(model);
					//model.removeRow(table.getSelectedRow());
					//DefaultListModel<String> defaultListModel = (DefaultListModel<String>)AppartmentTableModel.getModel();		
					/*
					for (Appartment temp : appartments) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(acceptguestforappartment.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}	
			}
		});
		addresscb.addActionListener(new ActionListener() 
		{
        	public void actionPerformed(ActionEvent event) 
        	{
        		try {
					String lastName = addresscb.getSelectedItem().toString();
					System.out.print(lastName);
					List<Appartment> appartmentes = null;
					if (lastName != null && lastName.trim().length() > 0) {
						appartments = AppartmentDAO.searchAppartmentsbyaddress(lastName);
					} else {
						appartments = AppartmentDAO.getAllAppartments();
					} 		
					AppartmentTableModel model = new AppartmentTableModel(appartments);
						table.setModel(model);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(acceptguestforappartment.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}	
        	}
		});
		
		
		jobcb.addActionListener(new ActionListener() 
		{
        	public void actionPerformed(ActionEvent event) 
        	{
        		try {
					String lastName = jobcb.getSelectedItem().toString();
					System.out.print(lastName);
					List<Appartment> appartments = null;
					if (lastName != null && lastName.trim().length() > 0) {
						appartments = AppartmentDAO.searchAppartmentsbyjobCourse(lastName);
					} else {
						appartments = AppartmentDAO.getAllAppartments();
					} 		
					AppartmentTableModel model = new AppartmentTableModel(appartments);
						table.setModel(model);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(acceptguestforappartment.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}	
        	}
		});
		
		
		
		
		coursecb.addActionListener(new ActionListener() 
		{
        	public void actionPerformed(ActionEvent event) 
        	{
        		try {
					String lastName = coursecb.getSelectedItem().toString();
					System.out.print(lastName);
					List<Appartment> appartments = null;
					if (lastName != null && lastName.trim().length() > 0) {
						appartments = AppartmentDAO.searchAppartmentsbyjobCourse(lastName);
					} else {
						appartments = AppartmentDAO.getAllAppartments();
					} 		
						AppartmentTableModel model = new AppartmentTableModel(appartments);
						table.setModel(model);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(acceptguestforappartment.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}	
        	}
		});
		
		
		sexcb.addActionListener(new ActionListener() 
		{
        	public void actionPerformed(ActionEvent event) 
        	{
        		try {
					String lastName = sexcb.getSelectedItem().toString();
					System.out.print(lastName);
					List<Appartment> appartments = null;
					if (lastName != null && lastName.trim().length() > 0) {
						appartments = AppartmentDAO.searchAppartmentbysex(lastName);
					} else {
						appartments = AppartmentDAO.getAllAppartments();
					} 		
						AppartmentTableModel model = new AppartmentTableModel(appartments);
						table.setModel(model);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(acceptguestforappartment.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}	
        	}
		});
		
		
		
		agecb.addActionListener(new ActionListener() 
		{
        	public void actionPerformed(ActionEvent event) 
        	{
        		try {
					String lastName = agecb.getSelectedItem().toString();
					System.out.print(lastName);
					List<Appartment> appartments = null;
					if (lastName != null && lastName.trim().length() > 0) {
						appartments = AppartmentDAO.searchAppartmentsbyage(lastName);
					} else {
						appartments = AppartmentDAO.getAllAppartments();
					} 		
						AppartmentTableModel model = new AppartmentTableModel(appartments);
						table.setModel(model);
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(acceptguestforappartment.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}	
        	}
		});
		
//Add to arraylist where users id is equal to username only display these rows		
/*Create button on form
Add selected rows equal to Arraylist
Create an arraylist step 1 where users id ="bob"
Create button display
where users id="bob"
Display Arraylist*/

		selectbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//loginusername password
				
				
				JTextField Addresstf = new JTextField();
				JTextField ApartmentNumbertf = new JTextField();
				JTextField Usernametf = new JTextField();
				JTextField Passwordtf = new JTextField();

				Object[] fields = {"Address: " , Addresstf,
						"Apartment Number: " , ApartmentNumbertf,
						"Username: " , Usernametf,
						"Password: " , Passwordtf};
				
				int decision = JOptionPane.showConfirmDialog(null, fields , "Update Dialog" , JOptionPane.OK_CANCEL_OPTION);
				
				
				try {
					String x = AppartmentDAO.selectAvailable(Addresstf.getText(), ApartmentNumbertf.getText(), Usernametf.getText(), Passwordtf.getText());
					String kj =	AppartmentDAO.check(x);
					System.out.print(kj);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				//
				//Select appartmentdetail variables * from accommodion_listings where username = loginusername
//				/and password = password
				//System.out.print get row
				//convert to string where this string is not null
				//get user where this row is equal to the got apartment
				
				//please enter apartment house and number
				//Select user from accommodation_listings where apartment house= apartment 
				//and house number = house number and login=login and password=password
				//return user if user!=null
				//{Select userdetails from accommodation_listing where username=user}
				//return userdetails
				//click yes/no
				//if myrst1.nxt() and yes clicked{
				//update set column=yes
				//if column=yes{
				//remove row from table
				
				
				//only allow 1 row to be selected by user check if username = user
				//in button for accept check 
			//}
			//}
			//else{column = false}
			//	
				
				
				
				//get apartment where login=x and password=y
				//else return have no apartment added
				
				//login in as administrator button for all forms requesting login
				//login check if username has apartment go to new form otherwise go to this form
				
				
				
				//where apartment=null and where login=x and password=y get extra user 

				//if user !=null return user show dialog delete apartment as you want to live with them 
				//and get this users details

				//otherwise return have no user
				//get variable accept check if gone through
			
				// table.convertRowIndexToModel(table.getSelectedRow()); 
			}});
		
		
		
		
		panel.add(btnSearch);
		panel.add(appartmentformbutton);
		
		panel.add(logoutexitbutton);
		panel.add(selectbutton);
		panel.add(addresscb);
		panel.add(sexcb);
		panel.add(agecb);
		panel.add(coursecb);
		panel.add(jobcb);
		
		//panel.add(scrollPane);	
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.SOUTH);
		
		contentPane.add(pan, BorderLayout.NORTH);
		
		
		contentPane.add(panel);
		table = new JTable();
		scrollPane.setViewportView(table);
	}
	
	
	
	
	public void refreshAppartmentsView() {

		try {
			List<Appartment> appartments = AppartmentDAO.getAllAppartments();

			// create the model and update the "table"
			AppartmentTableModel model = new AppartmentTableModel(appartments);

			table.setModel(model);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
}

