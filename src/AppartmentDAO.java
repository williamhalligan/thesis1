

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.*;
import java.sql.Date;
import java.awt.Component;
import java.io.*;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class AppartmentDAO {

	private static Connection myConn;
	
	public AppartmentDAO() throws Exception {
		
		// get db properties
		getDatabseConnection();
		
	}
	
	public List<Appartment> getAllAppartments() throws Exception {
		List<Appartment> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from accommodation_listings");
			
			while (myRs.next()) {
				Appartment tempAppartment = convertRowToAppartment(myRs);
				list.add(tempAppartment);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<Appartment> searchAppartments(String search) throws Exception {
		List<Appartment> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;


		try {
			search += "%";
			myStmt = myConn.prepareStatement("select * from accommodation_listings where "
					+ "landlords_first_Name like  ? or "
					+ "landlords_Surname like ? or "
					+ "fname like ? or "
					+ "lname like ? or "
					+ "username like ? or "
					+ "sex like ? or  "
					+ "jobORcourse like ? or "
					+ "address like ?");

					
			
			
			myStmt.setString(1, search);
			myStmt.setString(2, search);
			myStmt.setString(3, search);
			myStmt.setString(4, search);
			myStmt.setString(5, search);
			myStmt.setString(6, search);
			myStmt.setString(7, search);
			myStmt.setString(8, search);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Appartment tempAppartment = convertRowToAppartment(myRs);
				list.add(tempAppartment);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private Appartment convertRowToAppartment(ResultSet myRs) throws SQLException {
		
		int acc_id = myRs.getInt("acc_id");
		String address = myRs.getString("address");
		String house_num = myRs.getString("house_num");
		String fname = myRs.getString("fname");
		String num_of_Bedrooms_available = myRs.getString("num_of_Bedrooms_available");
		String num_Bedrooms_in_House = myRs.getString("num_Bedrooms_in_House");
		String rent_Per_Week_OR_Month = myRs.getString("rent_Per_Week_OR_Month");
		String deposit_required = myRs.getString("deposit_required");
		String landlords_first_Name = myRs.getString("landlords_first_Name");
		String landlords_Surname = myRs.getString("landlords_Surname");
		String mobile_Num = myRs.getString("mobile_Num");
		String lname = myRs.getString("lname");
		String username = myRs.getString("username");
		String password = myRs.getString("password");
		String sex = myRs.getString("sex");
		String jobORcourse = myRs.getString("jobORcourse");
		String gAccount = myRs.getString("gAccount");
		String age2 = myRs.getString("age");
		String user = myRs.getString("user");
		
		String gPassword = myRs.getString("gPassword");
		String paragraph_about_self = myRs.getString("paragraph_about_self");

		
		Appartment tempAppartment = new Appartment(acc_id, address, house_num, num_of_Bedrooms_available, 
				num_Bedrooms_in_House, rent_Per_Week_OR_Month, deposit_required, landlords_first_Name, 
				landlords_Surname, mobile_Num, fname, lname, username, password, sex, jobORcourse, 
				gAccount, age2,  gPassword, paragraph_about_self, user);
		
		return tempAppartment;
	}

	
	
	
	
	
	
	
	
	

	
	public void checkAvailable(int id, String login) throws Exception {
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {

				myStmt = myConn.prepareStatement("Select Accept from accommodation_listings where acc_id="+id
						+" and user='"+login+"'");
				myRs = myStmt.executeQuery();
				while (myRs.next()) {
					String acceptS = myRs.getString("Accept");	
					
					if (acceptS.equals("true"))
					{
						JOptionPane.showConfirmDialog(null, "The person who advertised this house has accepted you to live with them" , "Create Dialog" , JOptionPane.OK_CANCEL_OPTION);
					}
					else if (acceptS.equals("false")){
						JOptionPane.showConfirmDialog(null, "The person who advertised this house has not accepted you to live with them" , "Create Dialog" , JOptionPane.OK_CANCEL_OPTION);
					}
					else{
						JOptionPane.showConfirmDialog(null, "The person who advertised this house may not have seen your request to live with them yet" , "Create Dialog" , JOptionPane.OK_CANCEL_OPTION);
					}
			}
		}
		finally {
			close(myStmt);
		}

	}
	
	public void deleteAppartment(int id, String login) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("delete from accommodation_listings where acc_id='"+id+"' and Accept='true' and user='"+login+"'");
			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
	}	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public  List<Appartment> searchAppartmentsbyaddress(String search) throws Exception {
		List<Appartment> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {
			search += "%";
			myStmt = myConn.prepareStatement("select * from accommodation_listings where "
					+ "address like ?");

			myStmt.setString(1, search);			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Appartment tempAppartment = convertRowToAppartment(myRs);
				list.add(tempAppartment);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	
	
	
	
	
	public List<Appartment> searchAppartmentsbyjobCourse(String search) throws Exception {
		List<Appartment> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {
			search += "%";
			myStmt = myConn.prepareStatement("select * from accommodation_listings where "
					+ "jobORcourse like ?");

			myStmt.setString(1, search);			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Appartment tempAppartment = convertRowToAppartment(myRs);
				list.add(tempAppartment);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	
	public List<Appartment> searchAppartmentbysex(String search) throws Exception {
		List<Appartment> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {
			search += "%";
			myStmt = myConn.prepareStatement("select * from accommodation_listings where sex like ?");

			myStmt.setString(1, search);			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Appartment tempAppartment = convertRowToAppartment(myRs);
				list.add(tempAppartment);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	
	
	public List<Appartment> searchAppartmentsbyage(String search) throws Exception {
		List<Appartment> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {
			search += "%";
			myStmt = myConn.prepareStatement("select * from accommodation_listings where age like ?");

			myStmt.setString(1, search);			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Appartment tempAppartment = convertRowToAppartment(myRs);
				list.add(tempAppartment);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
    
	
	/*
	int x=0;
	try{
		Statement statement = connect.createStatement() ;
		//EmployeeDAO.CheckLogin(username, password);
		while(AppartmentDAO.CheckLogin(username,password).next() &&  x<=1){
			int nf = JOptionPane.showConfirmDialog(
					frame,
					"Username or Password taken",
					"Username or Password taken",
					JOptionPane.WARNING_MESSAGE);
			unique = false;
			x++;
		}
		if(unique){
		}
		*/
	public void updateAppartment(int id, String x, String address, String houseNum,
			String fname, String lname, String mobilenum, String loginusername) throws Exception 
	{
		PreparedStatement myStmt = null;
		boolean unique = true;
		int y=0;
		while(Checkuser(x).next() && y<=1)
		{
			int nf = JOptionPane.showConfirmDialog(
					null,
					"Can only  select 1 appartment",
					"Can only  select 1 appartment",
					JOptionPane.WARNING_MESSAGE);
		
			unique=false;
			y++;
		}
		if(unique)
		{
			try 
			{
				Checkuser(x);
			
				// prepare statement
				myStmt = myConn.prepareStatement("update accommodation_listings"
						+" set user='"+x+"' where acc_id="+id+" and user = 'N/A' and user!='"+x+"'");
		
			
				// execute SQL
				int e =myStmt.executeUpdate();
			
				if (e==0)
				{
					String d=checkisuserinrow(id);
					System.out.print("\n"+d+"\n");
					if(d.equals(loginusername))
					{
						JOptionPane.showConfirmDialog(
							null, "<html>This appartment is awaiting answer to your request for this appartment</html>", "Confirm", 
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					}
				else
				{
					JOptionPane.showConfirmDialog(
							null, "<html>Apartment no longer available has another user has selected it</html>", "Confirm", 
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				}
				}
			else if(e>=0)
			{
			JOptionPane.showMessageDialog(null,
					"Appartment confirmed request."+ 
					"\n\nAddress: " + address
					+ "\nHouse number: " + houseNum
					+ "\nLandlords Name: " + fname
					+ " " + lname
					+ "\nLandlords phone number: " + mobilenum, "Error",
					JOptionPane.INFORMATION_MESSAGE);
			
			
			System.out.print("\n"+e);
			}
		}
		finally 
		{
			close(myStmt);
		}
	}
}

	public String checkisuserinrow(int id) throws Exception {
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		String user="";
		try {
				myStmt = myConn.prepareStatement("Select user"
						+ " from accommodation_listings where acc_id='"+id+"'");
				myRs = myStmt.executeQuery();
				while (myRs.next()) {
					user = myRs.getString("user");							
			}
			return user;
		}
		finally {
			close(myStmt);
		}

	}
	
	
	

	
		public static  ResultSet Checkuser(String user) throws SQLException {	
		PreparedStatement myStmt=null;	
		try{
			myStmt=	myConn.prepareStatement("select * from accommodation_listings where "
					+ "`user`=?");

 		myStmt.setString(1, user);
		ResultSet rs = myStmt.executeQuery( );
		return rs;
	}
	finally {
		close(myStmt);
	}
	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String selectAvailable(String appartment, String appartmentnumber, 
			String username, String password) throws Exception {
		String x="";
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {
			//please enter apartment house and number
			//
			myStmt = myConn.prepareStatement("Select user from accommodation_listings "
			+ "where address = '"+appartment+"' and " 
			+"house_num = '"+appartmentnumber+"' and username = '"+username+"' and "
			+ "password = '"+password+"'");

			// execute SQL			
			myRs = myStmt.executeQuery();
			
		
			while (myRs.next()) {
				//System.out.print(myRs.getString("user"));
				x = myRs.getString("user");
					//close(myStmt, myRs);
			}
			String y= x;
			return y;
		}
		finally {
			close(myStmt, myRs);
			//close(myStmt, myRs2);
		}
	}
	
	
	public String check(String x) throws Exception {
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		String check1="";
		try {

			if(!x.equals("N/A")){

				myStmt = myConn.prepareStatement("Select acc_id, fname, lname, username, sex, jobORcourse, age, "
						+ "paragraph_about_self from accommodation_listings where user='"+x+"'");
				myRs = myStmt.executeQuery();
				while (myRs.next()) {
					String fnameS = myRs.getString("fname");	
					String lnameS = myRs.getString("lname");		
					String ageS = myRs.getString("username");		
					String sexS = myRs.getString("sex");		
					String jobCourseS = myRs.getString("jobORcourse");
					String bioS = myRs.getString("paragraph_about_self");
					int id = myRs.getInt("acc_id");


					//return userdetails		
					JLabel fname = new JLabel(fnameS);
					JLabel lname = new JLabel(lnameS);
					JLabel age = new JLabel(ageS);
					JLabel sex = new JLabel(sexS);
					JLabel jobCourse = new JLabel(jobCourseS);
					JLabel bio = new JLabel(bioS);
					//Used to generate the fields in the option pane.
					Object[] fields = {"Release Year: " , fname,
							"Title: " , lname,
							"Genre: " , age,
							"Director: " , sex,
							"Job or Course:", jobCourse,
							"Small bio", bio};

					int decision = JOptionPane.showConfirmDialog(null, fields , "Create Dialog" , JOptionPane.OK_CANCEL_OPTION);

					//For decision OK is 0, Cancel is 2 and Escape is -1.
					if(decision == 0)
					{
						updateAccept("true", id);
						//System.out.print("JIMMY");
						check1="1";
					}
					else{
						updateAccept("false", id);
						//System.out.print("JIMMY");
						check1 = "2";
						updateUser(id);
					}
					
				}
				
			}
			return check1;
		}
		finally {
			close(myStmt);
		}

	}
	public void updateAccept(String accept, int id) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("update accommodation_listings"
		+" set Accept=? where acc_id=?");
			
			// set params
			myStmt.setString(1, accept);
			myStmt.setInt(2, id);
			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	
	public static void checkhasAppartment(String login, String password) throws Exception {
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		try {
				myStmt = myConn.prepareStatement("Select address, house_num from accommodation_listings where username='"+login+"' and password='"+password+"'");
				myRs = myStmt.executeQuery();
				
				
				
				while (myRs.next()) {
					String apartmentaddress = myRs.getString("address");	
					String apartmenthousenum = myRs.getString("house_num");	
					if(!apartmentaddress.equals("n/a") && !apartmenthousenum.equals("n/a")){
						acceptguestforappartment cancellogin = new acceptguestforappartment(login, password);
						cancellogin.setVisible(true);
					}
					else{

						JOptionPane.showConfirmDialog(
								null, "You have not advertised an apartment", "Confirm", 
								JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					}
					
			}
			
		}
		finally {
			close(myStmt);
		}
	}
	
	
	
	
	
	
	public void updateUser(int id) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("update accommodation_listings"
		+" set User='N/A' where acc_id="+id);
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	

	//1
	//change button to other button on form
	
	//2
	//only allow 1 row to be selected by user check if username = user in button for accept check
	
	//3
	//last touch delete classes and edit rows in database

	//4
	//insert new column into table for is available
	//new combo box for is available
		
		
	
	

	
	
	
	
	
	
	public static void login(Adminlogin log, String tf1, String tf2) throws SQLException {
	PreparedStatement myStmt=null;
 	try{
 		
 		myStmt=	myConn.prepareStatement("Select * from accommodation_listings where username=? and password=?");
 		myStmt.setString(1, tf1);
		myStmt.setString(2, tf2);
		ResultSet rs = myStmt.executeQuery( );
		
if (tf1.equals("") || tf2.equals("")){
	Component frame = null;
	int n = JOptionPane.showConfirmDialog(
    	    frame,
    	    "Form not filled out incorrectly, please fill out all fields on form",
    	    "Form filled out incorrectly",
    	    JOptionPane.WARNING_MESSAGE);
}




         	
else if(rs.next())
         			{
         				log.dispose();
         				
         				AppartmentSearchApp look = new AppartmentSearchApp(tf1, tf2);
         		    	look.setVisible(true);
         				//Driver dbc = new Driver();
         				
         			}	
         			else{
                 		Component frame = null;
                		int n = JOptionPane.showConfirmDialog(
                        	    frame,
                        	    "User doesn't exist",
                        	    "User does not exist",
                        	    JOptionPane.WARNING_MESSAGE);
                 		
                 	}
         			
         		}catch(Exception exc){
         		exc.printStackTrace();	
         	} 	
	
	
 	}	
	

	
	
	
	
	
	
	
	
	
  	
	
	public static void login2(Adminlogin log, String tf1, String tf2) throws SQLException {
		PreparedStatement myStmt=null;
	 	try{
	 		
	 		myStmt=	myConn.prepareStatement("Select * from accommodation_listings where username=? and password=?");
	 		myStmt.setString(1, tf1);
			myStmt.setString(2, tf2);
			ResultSet rs = myStmt.executeQuery( );
			
	if (tf1.equals("") || tf2.equals("")){
		Component frame = null;
		int n = JOptionPane.showConfirmDialog(
	    	    frame,
	    	    "Form not filled out incorrectly, please fill out all fields on form",
	    	    "Form filled out incorrectly",
	    	    JOptionPane.WARNING_MESSAGE);
	}
	         	
	else if(rs.next())
	         			{
	         				log.dispose();
	         				
	         				houseform look = new houseform(tf1, tf2);
	         		    	look.setVisible(true);
	         				//Driver dbc = new Driver();
	         				
	         			}	
	         			else{
	                 		Component frame = null;
	                		int n = JOptionPane.showConfirmDialog(
	                        	    frame,
	                        	    "User doesn't exist",
	                        	    "User does not exist",
	                        	    JOptionPane.WARNING_MESSAGE);
	                 		
	                 	}
	         			
	         		}catch(Exception exc){
	         		exc.printStackTrace();	
	         	} 	
		
		
	 	}
	
	
	
	public static void login3(Adminlogin log, String tf1, String tf2) throws SQLException {
		PreparedStatement myStmt=null;
	 	try{
	 		
	 		myStmt=	myConn.prepareStatement("Select * from accommodation_listings where username=? and password=?");
	 		myStmt.setString(1, tf1);
			myStmt.setString(2, tf2);
			ResultSet rs = myStmt.executeQuery( );
			
	if (tf1.equals("") || tf2.equals("")){
		Component frame = null;
		int n = JOptionPane.showConfirmDialog(
	    	    frame,
	    	    "Form not filled out incorrectly, please fill out all fields on form",
	    	    "Form filled out incorrectly",
	    	    JOptionPane.WARNING_MESSAGE);
	}
	         	
	else if(rs.next())
	         			{
	         				log.dispose();
	         				
	         				chooseTask look = new chooseTask(tf1, tf2);
	         		    	look.setVisible(true);
	         				//Driver dbc = new Driver();
	         				
	         			}	
	         			else{
	                 		Component frame = null;
	                		int n = JOptionPane.showConfirmDialog(
	                        	    frame,
	                        	    "User doesn't exist",
	                        	    "User does not exist",
	                        	    JOptionPane.WARNING_MESSAGE);
	                 		
	                 	}
	         			
	         		}catch(Exception exc){
	         		exc.printStackTrace();	
	         	} 	
		
		
	 	}
	
 	
	public static void register(String fname, String lname, String username, String password,
			String sex, String jobCourse, String googleaccount, String googlepassword, String age, 
			String smallparadescribeself) throws SQLException {
		
		PreparedStatement myStmt=null;	
		try{
			myStmt=	myConn.prepareStatement("insert into accommodation_listings(address, house_num, num_of_Bedrooms_available, num_Bedrooms_in_House, "
				+ "rent_Per_Week_OR_Month,  deposit_required, landlords_first_Name, landlords_Surname, mobile_Num,"
				+ "fname, lname, username, password, sex, jobORcourse, gAccount, gPassword, age,paragraph_about_self, user,  accept) "
		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?, ?)");
		
 		String c="N/A";
 		myStmt.setString(1, c);
		myStmt.setString(2, c);
		myStmt.setString(3, c);
		myStmt.setString(4, c);
		myStmt.setString(5, c);
		myStmt.setString(6, c);
		myStmt.setString(7, c);
		myStmt.setString(8, c);
		myStmt.setString(9, c);
		
		myStmt.setString(10, fname);
		myStmt.setString(11, lname);
		myStmt.setString(12, username);
		myStmt.setString(13, password);
		myStmt.setString(14, sex);
		myStmt.setString(15, jobCourse);
		myStmt.setString(16, googleaccount);
		myStmt.setString(17, googlepassword);
		myStmt.setString(18, age);
		myStmt.setString(19, smallparadescribeself);
		myStmt.setString(20, c);
		myStmt.setString(21, c);
		myStmt.executeUpdate();			
	}
	finally {
		close(myStmt);
	}
	}

	
	
	
	
	
public static  ResultSet CheckLogin(String username, String password) throws SQLException {
		
		PreparedStatement myStmt=null;	
		try{
			myStmt=	myConn.prepareStatement("select * from accommodation_listings where "
					+ "`username`=? AND `password`=?");

 		myStmt.setString(1, username);
		myStmt.setString(2, password);
		ResultSet rs = myStmt.executeQuery( );
		return rs;
	}
	finally {
		close(myStmt);
	}
	}

public static void updateAppartment(String address, String housenum, String numrmsavailable, String numrmshouse,
		String costrent, String costdeposit, String landlordsfname, String landlordslname, 
		String landlordsphonenum, String tf1, String tf2) throws SQLException {
	
	PreparedStatement myStmt = null;
	try {
		// prepare statement
		myStmt = myConn.prepareStatement("update accommodation_listings set address=?, "
				+ "house_num=?, num_of_Bedrooms_available=?, num_Bedrooms_in_House=?, "
				+ "rent_Per_Week_OR_Month=?, deposit_required=?, landlords_first_Name=?, landlords_Surname=?, "
				+ "mobile_Num=? where username=? and password=?");
		
		// set params
		myStmt.setString(1, address);
		myStmt.setString(2, housenum);
		myStmt.setString(3, numrmsavailable);
		myStmt.setString(4, numrmshouse);
		myStmt.setString(5, costrent);
		myStmt.setString(6, costdeposit);
		myStmt.setString(7, landlordsfname);
		myStmt.setString(8, landlordslname);
		myStmt.setString(9, landlordsphonenum);
		myStmt.setString(10, tf1);
		myStmt.setString(11, tf2);
		
		// execute SQL
		myStmt.executeUpdate();			
	}
	finally {
		close(myStmt);
	}
	
}


	
	
	
	
	
	
	
	
	
	
	
	
	
	private static void close(Connection myConn, Statement myStmt, ResultSet myRs)
			throws SQLException {

		if (myRs != null) {
			myRs.close();
		}

		if (myStmt != null) {
			
		}
		
		if (myConn != null) {
			myConn.close();
		}
	}

	private void close(Statement myStmt, ResultSet myRs) throws SQLException {
		close(null, myStmt, myRs);		
	}

	private static void close(Statement myStmt) throws SQLException {
		close(null, myStmt, null);		
	}
	
	
	public static Connection getDatabseConnection() {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("demo.properties"));
			String user = props.getProperty("user");
			String password = props.getProperty("password");
			String dburl = props.getProperty("dburl");
			
			// connect to database
			try {
				myConn = DriverManager.getConnection(dburl, user, password);
				System.out.println("DB connection successful to: " + dburl);
				return myConn;
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
		return myConn;
		
		
	}

	public static void main(String[] args) throws Exception {
		
		AppartmentDAO dao = new AppartmentDAO();
		System.out.println(dao.searchAppartments("thom"));

		System.out.println(dao.getAllAppartments());
	}
}
